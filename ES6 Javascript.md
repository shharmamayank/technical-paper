* # Table of content
1. ES6
2. Data structure
  * types of data structure in javaScript
3. Data types
 * primitive data types
 * Non-primitive data types
4. Reference

1. # ES6
 # What is ES6 ?
ES6 is also known as ECMAScript 6 and ECMAScript 2015.
Some people call it JavaScript 6.
ECMAScript(ES) is a standardised scripting language for JavaScript (JS). The current ES version supported in modern browsers is ES5. However, ES6 tackles a lot of the limitations of the core language, making it easier for devs to code.
New Features in ES6.
* JavaScript let
* JavaScript const
* avaScript Arrow Functions
* JavaScript Classes
* Default parameter values
* Array.find()
* Array.findIndex()
* Exponentiation (**) (EcmaScript 2016)

2. # Data Structure in javaScript
   # What is a data structure?
In computer science, a data structure is a format to organize, manage and store data in a way that allows efficient access and modification.
More precisely, a data structure is a collection of data values, the relationships among them, and the functions or operations that can be applied to that data.

These definitions might sound a bit abstract at first, but think about it. If you've been coding for a little while, you must have used data structures before.

Have you used arrays and objects? Those are all data structures. All of them are a collection of values that relate to each other, and can be operated on by you.

# 8 COMMON DATA STRUCTURES IN JAVASCRIPT
 * Stack
 * Queue
 * Linked List
 * Set
 * Hash Table
 * Tree
 * Trie
 * Graph

 1. # Stack
   Stack follows the principle of LIFO (last In, first out). If you stack books, the top book will be taken before the bottom one. When you browse on the internet, the back button leads you to the most recently browsed page.

# COMMON METHODS OF STACK IN JAVASCRIPT
push: Input a new element.
pop : Remove the top element, return the removed element.
peek : Return the top element.
length : Return the number of element(s) in the stack.
Array in JavaScript has the attributes of a stack, but we construct a stack from scratch by using function Stack().

## SAMPLE CODE
function Stack() {
this.count = 0;
  this.storage = {};

  this.push = function (value) {
    this.storage[this.count] = value;
    this.count++;
  }

  this.pop = function () {
    if (this.count === 0) {
      return undefined;
    }
    this.count--;
    var result = this.storage[this.count];
    delete this.storage[this.count];
    return result;
  }

  this.peek = function () {
    return this.storage[this.count - 1];
  }

  this.size = function () {
    return this.count;
  }
}

2. # Queue
Queue is similar to stack. The only difference is that queue uses the FIFO principle (first in, first out). In other words, when you queue for the bus, the first in the queue will always board first.

# QUEUE METHODS IN JAVASCRIPT

enqueue: Enter queue, add an element at the end.
dequeue: Leave queue, remove the front element and return it.
front: Get the first element.
isEmpty: Determine whether the queue is empty.
size: Get the number of element(s) in queue.

Array in JavaScript has some attributes of queue, so we can use array to construct an example for queue:

## SAMPLE CODE for QUEUE

function Queue() {
  var collection = [];
  this.print = function () {
    console.log(collection);
  }
  this.enqueue = function (element) {
    collection.push(element);
  }
  this.dequeue = function () {
    return collection.shift();
  }
  this.front = function () {
    return collection[0];
  }

  this.isEmpty = function () {
    return collection.length === 0;
  }
  this.size = function () {
    return collection.length;
  }
}

3. # Linked List

 A linked list is a chained data structure. Each node consists of two pieces of information: the data of the node and the pointer to the next node. Linked list and conventional array are both linear data structures with serialized storage.

 UNILATERAL LINKED LIST METHODS
size: Return the number of node(s).
head: Return the element of the head.
add: Add another node in the tail.
remove: Remove a certain node.
indexOf: Return the index of a node.
elementAt: Return the node of an index.
addAt: Insert a node at a specific index.
removeAt: Delete a node at a specific index.

 4. # Set
  A set is a basic concept in mathematics: a collection of well defined and distinct objects. ES6 introduced the concept of set, which has some similarities to  an array. However, a set does not allow repeating elements and is not indexed.

  # TYPICAL SET METHODS IN JAVASCRIPT
values: Return all elements in a set.
size: Return the number of elements.
has: Determine whether an element exists.
add: Insert elements into a set.
remove: Delete elements from a set.
union: Return the intersection of two sets.
difference: Return the difference of two sets.
subset: Determine whether a certain set is a subset of another set.

## SAMPLE CODE FOR SET

function MySet() {  
    var collection = [];  
    this.has = function (element) {    
        return (collection.indexOf(element) !== -1);  
    }  
    this.values = function () {    
        return collection;  
    }  
    this.size = function () {    
        return collection.length;  
    }  
    this.add = function (element) {    
        if (!this.has(element)) {      
            collection.push(element);      
            return true;    
        }    
        return false;  
    }  

5. # Hash Table

A hash table is a key-value data structure. Due to the lightning speed of querying a value through a key, hash tables are commonly used in map, dictionary or object data structures. As shown in the graph above, the hash table uses a hash function to convert keys into a list of numbers, and these numbers serve as the values of corresponding keys. To get value using a key is fast; time complexity can achieve O(1). The same keys must return the same values, which is the basis of the hash function.

# HASH TABLE METHODS IN JAVASCRIPT
add: Add a key-value pair.
remove: Delete a key-value pair.
lookup: Find a corresponding value using a key.

An example of a simplified hash table in JavaScript:

function hash(string, max) {
  var hash = 0;
  for (var i = 0; i < string.length; i++) {
    hash += string.charCodeAt(i);
  }
  return hash % max;
}

function HashTable() {
  let storage = [];
  const storageLimit = 4;

  this.add = function (key, value) {
    var index = hash(key, storageLimit);
    if (storage[index] === undefined) {
      storage[index] = [
        [key, value]
      ];
    } else {
      var inserted = false;


 6. # Tree

      Tree data structure is a non-linear multi-layer data structure in contrast to array, stack and queue. This structure is highly efficient during insert and search operations. Let’s take a look at some concepts of tree data structure:


  # TREE DATA STRUCTURE CONCEPTS
root: Root node of a tree; no parent node for root.
parent node: Direct node of the upper layer; only has one
child node: Direct node(s) of the lower layer; can have multiple
siblings: Share the same parent node
leaf: Node with no child
Edge: Branch or link between nodes
Path: The edges from a starting node to the target node
Height of Node: Number of edges of the longest path of a specific node to leaf node
Height of Tree: Number of edges of the longest path of the root node to the leaf node
Depth of Node: Number of edges from root node to specific node
Degree of Node: Number of child nodes
Each node has a maximum of two nodes with the left node being smaller than the current node and the right node being bigger than the current node.

COMMON METHODS OF BINARY SEARCH TREE IN JAVASCRIPT
add: Insert a node into the tree.
findMin: Get the minimum node.
findMax: Get the maximum node.
find: Search a specific node.
isPresent: Determine the existence of a certain node.
remove: Delete a node from the tree.

7. # Trie 
Trie (pronounced “try”) or “prefix tree” is also a type of search tree. Trie stores the data step-by-step; each node in the tree represents a step. We use trie to store vocabulary so it can be quickly searched, especially for an auto-complete function.

Each node in trie has an alphabet and following the branch can form a complete word. It also comprises a boolean indicator to show whether or not it’s the end of a string.

# METHODS OF TRIE IN JAVASCRIPT
add: Insert a word into the dictionary tree.
isWord: Determine whether the tree consists of a certain word.
print: Return all words in the tree.

## SAMPLE CODE FOR TRIE

const TrieNode = function (key) {
  this.key = key;
  
  this.parent = null;
  
  this.children = {};
  
  this.end = false;
  
  this.getWord = function() {
    let output = [];
    let node = this;

    while (node !== null) {
      output.unshift(node.key);
      node = node.parent;
    }

    return output.join('');
  };
}

const Trie = function() {
  this.root = new TrieNode(null);

}

8. # Graph
 Graphs, sometimes known as networks, refer to sets of nodes with linkages (or edges). We can further divide graphs into two groups (i.e. directed graphs and undirected graphs), according to whether the linkages have direction. We use graphs in our daily lives without even realizing it. Graphs help calculate the best route in navigation apps or recommend friends with whom we might like to connect.




 # DATA TYPE 
A value in JavaScript is always of a certain type. For example, a string or a number.

There are eight basic data types in JavaScript. Here, we’ll cover them in general and in the next chapters we’ll talk about each of them in detail.

We can put any type in a variable. Data types are of two types Primitive Data Type and Non-Primitive Data Type 

 # Primitive Data Type
 * String
 * BigInt
 * Numbers
 * Undefined
 * Boolean
 * Symbol
 * Null

# NUMBERS

 The number type represents both integer and floating point numbers.
There are many operations for numbers, e.g. multiplication *, division /, addition +, subtraction -, and so on.
Example-
let n = 123;
n = 12.345;

# BigInt

n JavaScript, the “number” type cannot safely represent integer values larger than (253-1) (that’s 9007199254740991), or less than -(253-1) for negatives.

To be really precise, the “number” type can store larger integers (up to 1.7976931348623157 * 10308), but outside of the safe integer range ±(253-1) there’ll be a precision error, because not all digits fit into the fixed 64-bit storage. So an “approximate” value may be stored.
So to say, all odd integers greater than (253-1) can’t be stored at all in the “number” type.
For most purposes ±(253-1) range is quite enough, but sometimes we need the entire range of really big integers, e.g. for cryptography or microsecond-precision timestamps.
BigInt type was recently added to the language to represent integers of arbitrary length.
A BigInt value is created by appending n to the end of an integer:

# STRINGS

A string in JavaScript must be surrounded by quotes.

let str = "Hello";
let str2 = 'Single quotes are ok too';
let phrase = `can embed another ${str}`;
In JavaScript, there are 3 types of quotes.

Double quotes: "Hello".
Single quotes: 'Hello'.
Backticks: `Hello`.
Double and single quotes are “simple” quotes. There’s practically no difference between them in JavaScript.

Backticks are “extended functionality” quotes. They allow us to embed variables and expressions into a string by wrapping them in ${…}, for example:

let name = "John";
alert( `Hello, ${name}!` ); 
alert( `the result is ${1 + 2}` ); 
The expression inside ${…} is evaluated and the result becomes a part of the string. We can put anything in there: a variable like name or an arithmetical expression like 1 + 2 or something more complex.

# BOOLEAN

The boolean type has only two values: true and false.
This type is commonly used to store yes/no values: true means “yes, correct”, and false means “no, incorrect”.
For EXAMPLE:
let nameFieldChecked = true; // yes, name field is checked
let ageFieldChecked = false; // no, age field is not checked
Boolean values also come as a result of comparisons:

let isGreater = 4 > 1;

alert( isGreater ); // true (the comparison result is "yes")

# NULL

The special null value does not belong to any of the types described above.
It forms a separate type of its own which contains only the null value:
let age = null;
In JavaScript, null is not a “reference to a non-existing object” or a “null pointer” like in some other languages.
It’s just a special value which represents “nothing”, “empty” or “value unknown”.
The code above states that age is unknown.

# UNDIFINED
The special value undefined also stands apart. It makes a type of its own, just like null.
The meaning of undefined is “value is not assigned”.
If a variable is declared, but not assigned, then its value is undefined:
let age;
alert(age); 
Technically, it is possible to explicitly assign undefined to a variable:

let age = 100;
age = undefined;

alert(age); 

# NON-PRIMITIVE TYPE
The object type is special.

All other types are called “primitive” because their values can contain only a single thing (be it a string or a number or whatever). In contrast, objects are used to store collections of data and more complex entities.

Being that important, objects deserve a special treatment. We’ll deal with them later in the chapter Objects, after we learn more about primitives.

The symbol type is used to create unique identifiers for objects. We have to mention it here for the sake of completeness, but also postpone the details till we know objects.

# Summary
There are 8 basic data types in JavaScript.

Seven primitive data types:
number for numbers of any kind: integer or floating-point, integers are limited by ±(253-1).
bigint for integer numbers of arbitrary length.
string for strings. A string may have zero or more characters, there’s no separate single-character type.
boolean for true/false.
null for unknown values – a standalone type that has a single value null.
undefined for unassigned values – a standalone type that has a single value undefined.
symbol for unique identifiers.
And one non-primitive data type:
object for more complex data structures.


# REFERENCES
* https://javascript.info/types
* https://www.javascripttutorial.net/javascript-data-types/
* https://builtin.com/software-engineering-perspectives/javascript-data-structures
